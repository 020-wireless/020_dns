#!/bin/bash 
#set -e
. paths.sh

HOSTS="bprouter kalium sofrouter"

cz ()
{
  # copies the zonefiles
  #  $1 name
  #  $2 reverse
  echo "copying $1 zone, with subnet $2"
  cd $OUT/$1.020
  scp db.$1 db.$2  root@$H:/etc/bind/zones/
  cd $OUT
}



make_zone () {
  # 1 hostfile.irl
  # 2 irl.020
  # 3 10.205.12:255.255.0.0 (don't put leading 0's in subnet)
  # 4
  hostfile=$1
  zone_name=$2
  subnet=$3

  mkdir $OUT/$zone_name
  cd $OUT/$zone_name && rm *

  $BIN/h2n -d $zone_name \
    -m 10:mail.$zone_name \
    -n $subnet      \
    -h 10.205.253.1   \
    -s 10.205.253.1   \
    -v 8 -y           \
    -H $HFD/$hostfile    \
    -u wireless-amsterdam@lists.puscii.nl   \
    -M -t

  shortname="!todo!"
  echo "made: "
  ls -lh $OUT/$zone_name
  echo "   $OUT/$zone_name/db.$shortname"
  echo "   $OUT/$zone_name/db.$shortname"

  cd $OUT

}

#routers /radios /etc.
make_zone routers routers.020 10.205.253:255.255.255.0
make_zone transfernet transfernet.020 10.205.255:255.255.255.0
#is same subnet as transfernet
#make_zone hostfile.radio radio.020 10.205.255:255.255.255.0

# acta vlan1
make_zone vlan1acta vlan1acta.020 192.168.88:255.255.255.0
make_zone amsw amsw.020 10.205.252:255.255.255.0


#
# irl.squesh.net zone
# 

make_zone irl irl.020 10.205.12:255.255.255.0:
make_zone irl irl.squesh.net 10.205.12:255.255.255.0

#ki zone
make_zone ki ki.020 10.205.14:255.255.255.0
make_zone ki ki.squesh.net 10.205.14:255.255.255.0

make_zone wfc wfc.020 10.205.9:255.255.255.0


