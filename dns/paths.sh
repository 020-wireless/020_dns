#
# vars
#
REPO="$(git rev-parse --show-toplevel)"

OUT=$REPO/dns/output/
BIN=$REPO/dns/
HFD=$REPO/dns/hostfiles/
STA=$REPO/dns/staticzones/

export OUT
export BIN
export HFD
